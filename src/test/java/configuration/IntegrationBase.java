package configuration;

import com.jayway.restassured.RestAssured;
import org.testng.annotations.BeforeMethod;

public class IntegrationBase {

    public static final String BASIC_URL_REMIT_WEB_SITE;

    static {
        BASIC_URL_REMIT_WEB_SITE = "http://remit.se/";
    }

    @BeforeMethod
    public void setUp() {
        RestAssured.basePath = "";
    }

}