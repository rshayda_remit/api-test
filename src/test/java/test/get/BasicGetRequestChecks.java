package test.get;

import com.jayway.restassured.response.Response;
import configuration.IntegrationBase;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.given;


public class BasicGetRequestChecks extends IntegrationBase {

    //simple test example to check if GET returns 200
    @Test
    public void simpleTestToCheckIfSiteWorks() {
        Response resp = given().when().
                get(BASIC_URL_REMIT_WEB_SITE);
        System.out.println("Result: " + resp.asString());
        Assert.assertEquals(200, resp.getStatusCode(), "Something went wrong, status code should be 200");
    }
}
