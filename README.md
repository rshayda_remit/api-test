## Setting up REST Assured
This is the example of simple GET test for remit.se website

## Tools needed:

 - Java 8 or Java 9 [ http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html ]
 - Maven  [ http://maven.apache.org/ ]
 - TestNG
 - RestAssured [ https://github.com/rest-assured/rest-assured/wiki/GettingStarted ]
 - some IDE [ I prefer IntelliJ IDEA ]

## Getting Started
1) Open the terminal / console and Initialize the project using GIT; then clone the project using below command on the target directory: git clone https://dimencionrider7@bitbucket.org/dimencionrider7/api-test.git
2) Once the project is cloned successfully open the terminal and/or console and execute the tests using maven.